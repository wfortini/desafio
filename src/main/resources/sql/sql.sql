create table paciente
    (id bigint not null auto_increment,
    nome varchar(255),
    cpf varchar(14),
    telefone varchar(15),
    idade varchar (3),
    primary key (id) );
	
	create table usuario (
     id bigint not null auto_increment,
      email varchar(255),
      login varchar(255),
      nome varchar(255),
      senha varchar(255),      
      primary key (id)
  );
  
  create table role (
   id bigint not null auto_increment,
    nome varchar(255),
    primary key (id)
);

create table usuario_roles (
   usuario_id bigint not null,
   role_id bigint not null
);

alter table usuario_roles
   add constraint FK_usuario_roles_role
   foreign key (role_id)
   references role (id);

alter table usuario_roles
   add constraint FK_usuario_roles_user
   foreign key (usuario_id)
   references usuario (id);
   
   insert into role(id,nome) values (1, 'ROLE_USAURIO');
insert into role(id,nome) values (2, 'ROLE_ADMIN');

insert into usuario(id,nome,email,login,senha) values (1,'Wellington','wellington@gmail.com','wellington','$2a$10$HKveMsPlst41Ie2LQgpijO691lUtZ8cLfcliAO1DD9TtZxEpaEoJe');
insert into usuario(id,nome,email,login,senha) values (2,'Admin','admin@gmail.com','admin','$2a$10$HKveMsPlst41Ie2LQgpijO691lUtZ8cLfcliAO1DD9TtZxEpaEoJe');
insert into usuario(id,nome,email,login,senha) values (3,'User','user@gmail.com','user','$2a$10$HKveMsPlst41Ie2LQgpijO691lUtZ8cLfcliAO1DD9TtZxEpaEoJe');

insert into usuario_roles(usuario_id,role_id) values(1, 1);
insert into usuario_roles(usuario_id,role_id) values(2, 2);
insert into usuario_roles(usuario_id,role_id) values(3, 1);

create table agendamento
    (id bigint not null auto_increment,
	dataHoraAgendamento datetime,
    primary key (id) );
	
create table usuario_agendamentos (
   usuario_id bigint not null,
   agendamento_id bigint not null
);

alter table usuario_agendamentos
   add constraint FK_usuario_agendamentos_agendamento
   foreign key (agendamento_id)
   references role (id);

alter table usuario_agendamentos
   add constraint FK_usuario_agendamentos_usuario
   foreign key (usuario_id)
   references usuario (id);

