package conexa.saude.desafio.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import conexa.saude.desafio.api.exception.ObjectNotFoundException;
import conexa.saude.desafio.domain.Paciente;
import conexa.saude.desafio.domain.PacienteRepository;
import conexa.saude.desafio.dto.PacienteDTO;

@Service
public class PacienteService {
	
	@Autowired
    private PacienteRepository rep;

    public List<PacienteDTO> getPacientes() {
        List<PacienteDTO> list = rep.findAll().stream().map(PacienteDTO::create).collect(Collectors.toList());
        return list;
    }
    
    public PacienteDTO getPacienteById(Long id) {
        Optional<Paciente> Paciente = rep.findById(id);
        return Paciente.map(PacienteDTO::create).orElseThrow(() -> new ObjectNotFoundException("Paciente não encontrado"));
    }   

    public PacienteDTO insert(Paciente Paciente) {
        Assert.isNull(Paciente.getId(),"Não foi possível inserir o registro");

        return PacienteDTO.create(rep.save(Paciente));
    }

    public PacienteDTO update(Paciente Paciente, Long id) {
        Assert.notNull(id,"Não foi possível atualizar o registro");

        // Busca o Paciente no banco de dados
        Optional<Paciente> optional = rep.findById(id);
        if(optional.isPresent()) {
            Paciente db = optional.get();
            // Copiar as propriedades
            db.setNome(Paciente.getNome());            
            System.out.println("Paciente id " + db.getId());

            // Atualiza o Paciente
            rep.save(db);

            return PacienteDTO.create(db);
        } else {
            return null;
            //throw new RuntimeException("Não foi possível atualizar o registro");
        }
    }

    public void delete(Long id) {
        rep.deleteById(id);
    }

}
