package conexa.saude.desafio.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import conexa.saude.desafio.domain.Paciente;
import conexa.saude.desafio.dto.PacienteDTO;
import conexa.saude.desafio.services.PacienteService;

@RestController
@RequestMapping("/api/v1/saude/pacientes")
public class PacienteController {

	@Autowired
	private PacienteService service;
	
	@GetMapping()
    public ResponseEntity get() {
        List<PacienteDTO> pacientes = service.getPacientes();
        return ResponseEntity.ok(pacientes);
    }
	
	@PostMapping
	public ResponseEntity post(@RequestBody Paciente paciente) {
	
	    PacienteDTO c = service.insert(paciente);
	
	    URI location = getUri(c.getId());
	    return ResponseEntity.created(location).build();
	}
	
	private URI getUri(Long id) {
	    return ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
	            .buildAndExpand(id).toUri();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity put(@PathVariable("id") Long id, @RequestBody Paciente paciente) {
	
	    paciente.setId(id);
	
	    PacienteDTO c = service.update(paciente, id);
	
	    return c != null ?
	            ResponseEntity.ok(c) :
	            ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable("id") Long id) {
	    service.delete(id);
	
	    return ResponseEntity.ok().build();
	}
}
