package conexa.saude.desafio.api.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import conexa.saude.desafio.domain.Usuario;
import conexa.saude.desafio.domain.UsuarioRepository;

@Service(value = "userDetailsService")
@Transactional
public class UsuarioDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UsuarioRepository rep;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Usuario usuario = rep.findByLogin(username);
        if(usuario == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return usuario;
    }
}
