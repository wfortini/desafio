package conexa.saude.desafio.api;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import conexa.saude.desafio.domain.Agendamento;
import conexa.saude.desafio.domain.Usuario;
import conexa.saude.desafio.domain.UsuarioRepository;
import conexa.saude.desafio.dto.AgendamentoDTO;


@RestController
@RequestMapping("/api/v1/saude/agendamentos")
public class MedioController {
	
	@Autowired
    private UsuarioRepository rep;
	
	@PostMapping
	public ResponseEntity post(@RequestBody AgendamentoDTO agendamento) {
	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = (Usuario) auth.getPrincipal();
		agendamento.setMedicoId(user.getId());
		
		Optional<Usuario> medico = rep.findById(user.getId());
		if(medico.isPresent()) {
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(agendamento.getDataHoraAgendamento(), formatter);		
			
			Agendamento agenda = new Agendamento();
			agenda.setDataHoraAgendamento(dateTime);
			agenda.setPacienteId(agendamento.getPacienteId());
			
			medico.get().addAgendamento(agenda);
			rep.save(medico.get());			
		}
	   
		URI location = getUri(user.getId());
	    return ResponseEntity.created(location).build();
	}
	
	private URI getUri(Long id) {
	    return ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
	            .buildAndExpand(id).toUri();
	}

}
