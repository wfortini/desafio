package conexa.saude.desafio.dto;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import conexa.saude.desafio.domain.Usuario;

public class UsuarioDTO {
	
	private String login;
    private String medico;    
    private String token;
    
    private List<AgendamentoDTO> agendamentos = new ArrayList();
    
    public void addAgendamento(AgendamentoDTO ag) {
    	this.agendamentos.add(ag);
    }

    public List<AgendamentoDTO> getAgendamentos() {
		return agendamentos;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	

	public String getMedico() {
		return medico;
	}

	public void setMedico(String medico) {
		this.medico = medico;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}	
	
	public UsuarioDTO() {
		// TODO Auto-generated constructor stub
	}
	
    public UsuarioDTO(Usuario usuario) {
		super();
		this.login = usuario.getUsername();
		this.medico = usuario.getNome();			
	}

	public static UsuarioDTO create(Usuario usuario, String token) {        
        UsuarioDTO dto = new UsuarioDTO(usuario);
        dto.token = token;        
        return dto;
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper m = new ObjectMapper();
        return m.writeValueAsString(this);
    }

}
