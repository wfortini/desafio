package conexa.saude.desafio.dto;

import java.time.LocalDateTime;

public class AgendamentoDTO {
	
	private long pacienteId;
	private String dataHoraAgendamento;
	private long medicoId;	
	
	public long getMedicoId() {
		return medicoId;
	}
	public void setMedicoId(long medicoId) {
		this.medicoId = medicoId;
	}
	public long getPacienteId() {
		return pacienteId;
	}
	public void setPacienteId(long pacienteId) {
		this.pacienteId = pacienteId;
	}
	public String getDataHoraAgendamento() {
		return dataHoraAgendamento;
	}
	public void setDataHoraAgendamento(String dataHoraAgendamento) {
		this.dataHoraAgendamento = dataHoraAgendamento;
	}
	
	

}
