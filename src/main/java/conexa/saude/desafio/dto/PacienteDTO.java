package conexa.saude.desafio.dto;

import conexa.saude.desafio.domain.Paciente;

public class PacienteDTO {
	
	private Long id;
    private String nome;
    private String cpf;
    private String idade;
    private String telefone;    
    
    public PacienteDTO() {
		// TODO Auto-generated constructor stub
	}    
    
	public PacienteDTO(Paciente paciente) {
		super();
		this.id = paciente.getId();
		this.nome = paciente.getNome();
		this.cpf = paciente.getCpf();
		this.idade = paciente.getIdade();
		this.telefone = paciente.getTelefone();
	}
	
	public static PacienteDTO create(Paciente paciente) {       
        return new PacienteDTO(paciente);
    }
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getIdade() {
		return idade;
	}
	public void setIdade(String idade) {
		this.idade = idade;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
    
    

}
