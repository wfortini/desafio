package conexa.saude.desafio.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Agendamento {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

	    private LocalDateTime dataHoraAgendamento;
	    private long pacienteId;    
	    

		public long getPacienteId() {
			return pacienteId;
		}

		public void setPacienteId(long pacienteId) {
			this.pacienteId = pacienteId;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public LocalDateTime getDataHoraAgendamento() {
			return dataHoraAgendamento;
		}

		public void setDataHoraAgendamento(LocalDateTime dataHoraAgendamento) {
			this.dataHoraAgendamento = dataHoraAgendamento;
		}
	    
		@ManyToMany(mappedBy = "agendamentos")
	    private List<Usuario> usuarios = new ArrayList<>();
		
		public void addUsuario(Usuario ag) {
			usuarios.add(ag);		    
		}

		public List<Usuario> getUsuarios() {
			return usuarios;
		}
		
	    

}
