package conexa.saude.desafio.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface  PacienteRepository extends JpaRepository<Paciente, Long>{

}
